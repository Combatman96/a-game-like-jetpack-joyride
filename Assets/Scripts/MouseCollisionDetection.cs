using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseCollisionDetection : MonoBehaviour
{
    private bool _isDead = false;
    public uint coinCount = 0;
    public Text countCountText;

    public EndGameHandler endgameHadler;

    public AudioSource soundCoinSFX;

    private void Start()
    {
        countCountText.text = "0";
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            Debug.Log("Got coin");
            coinCount++;
            countCountText.text = "" + coinCount;
            
            soundCoinSFX.Play();
            
            Destroy(other.gameObject);
        }
        else
        {
            _isDead = true;
            //show menu
            endgameHadler.ShowMenu();

            AudioSource soundLazerSfx = other.gameObject.GetComponent<AudioSource>();
            soundLazerSfx.Play();
        }
    }

    public bool GetIsDead()
    {
        return _isDead;
    }
}
