using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AnimationController : MonoBehaviour
{
    public Animator animator;
    public bool isGrounded;
    public Transform goundCheck;
    public LayerMask groundLayerMask;

    public ParticleSystem jetpack;

    private MouseCollisionDetection _collisionDetection;
    private bool isDead;
    private void Start()
    {
        _collisionDetection = GetComponent<MouseCollisionDetection>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckIsGrounded();

        JetPackControll (Input.GetButton("Fire1"));

        isDead = _collisionDetection.GetIsDead();
        if(isDead) animator.SetBool("isDead",isDead);
    }

    void CheckIsGrounded()
    {
        isGrounded = Physics2D.OverlapCircle(goundCheck.position, 0.1f, groundLayerMask);
        animator.SetBool("isGrounded", isGrounded);
    }

    void JetPackControll(bool jetpackActive)
    {
        var jetpackEmission = jetpack.emission;
        jetpackEmission.enabled = !isGrounded;
        if (jetpackActive)
        {
            jetpackEmission.rateOverTime = 370.0f;
        }
        else
        {
            jetpackEmission.rateOverTime = 75.0f;
        }
    }

}
