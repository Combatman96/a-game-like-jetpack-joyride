using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target ; 
    public float offset = -3.5f;


    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = new Vector3(target.position.x + offset, this.transform.position.y , this.transform.position.z);
        this.transform.position = newPos;
    }
}
