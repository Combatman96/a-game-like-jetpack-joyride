using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiceController : MonoBehaviour
{
    public float jetpackForce = 75.0f;
    private Rigidbody2D playerRigidbody;
    private bool jetpackActive;
    public float movementSpeed = 3f;

    private MouseCollisionDetection _collisionDetection;
    private bool isDead;

    public AudioSource footstepSfx;
    public AudioSource jetpackSfx;

    private bool isGround;
    public AnimationController animationController;

    public ParallaxScrol paralax;
    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        _collisionDetection = GetComponent<MouseCollisionDetection>();
    }

    // Update is called once per frame
    void Update()
    {
        isDead = _collisionDetection.GetIsDead();
        if(!isDead)
            jetpackActive = Input.GetButton("Fire1");
        else
        {
            jetpackActive = false;
        }
        
        ChangeSoundStepAndJet(jetpackActive);

        paralax.offset = this.transform.position.x;
    }

    void FixedUpdate() 
    {
        if (jetpackActive)
        {
            playerRigidbody.AddForce(new Vector2(0, jetpackForce));
        }

        if (!isDead)
        {
            Vector2 playerVelocity = playerRigidbody.velocity;
            playerRigidbody.velocity = new Vector2(movementSpeed, playerVelocity.y);
        }

    }

    void ChangeSoundStepAndJet(bool jetpackActive)
    {
        isGround = animationController.isGrounded;
        footstepSfx.enabled = (!isDead && isGround);
        jetpackSfx.enabled = (!isDead && !isGround);

        if (jetpackActive)
        {
            jetpackSfx.volume = 1.0f;
        }
        else
        {
            jetpackSfx.volume = 0.50f;
        }
    }

}
