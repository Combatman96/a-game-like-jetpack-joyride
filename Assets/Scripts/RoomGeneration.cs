using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RoomGeneration : MonoBehaviour
{
    public Transform playerTransform;
    public GameObject[] availableRooms;
    public List<GameObject> currentRooms;
    private float screenWidthInPoints;

    // Use this for initialization
    private void Start()
    {
        float height = 2.0f * Camera.main.orthographicSize;
        screenWidthInPoints = height * Camera.main.aspect;
        
        StartCoroutine(GeneratorCheck());
    }

    
    private IEnumerator GeneratorCheck()
    {
        while (true)
        {
            GenerateRoomIfRequired();
            yield return new WaitForSeconds(0.25f);
        }
    }


    void AddRoom(float furthestRoomEndX)
    {
        //Instantiate a random room to scene
        int randomIndex = Random.Range(0, availableRooms.Length);
        GameObject room = (GameObject)Instantiate(availableRooms[randomIndex]);

        //Set position to that room
        float roomWidth = room.transform.Find("Floor").localScale.x;

        //It will be at the end of the row of rooms already in the scene
        float roomNewPosX = furthestRoomEndX + (roomWidth * 0.5f);

        room.transform.position = new Vector3(roomNewPosX, 0, 0);
        
        currentRooms.Add(room);
    }

    void GenerateRoomIfRequired()
    {
        /*
            Let say there are two screen-size boxs in front and back of the Player
            The end point of the box in front will be the spawn threadhold
            The start point of the box behind will be the despawn threadhold
        */

        float playerPosX = playerTransform.position.x;

        float spawnThreadhold = screenWidthInPoints + playerPosX;
        float despawnThreadhold = playerPosX - screenWidthInPoints;

        float furthestRoomEndX = 0f;

        List<GameObject> removeRooms = new List<GameObject>();
        
        bool canLoadRoom = true;

        foreach(var room in currentRooms)
        {
            /*
                If the start point of the room is not infront of spawn threadhold then no need to load that room
                If the end of the room is in front of the despawn threadhold then delete that room
            */

            float roomWidth = room.transform.Find("Floor").localScale.x;

            float startRoomPosX = room.transform.position.x - (roomWidth * 0.5f);
            float endRoomPosX = startRoomPosX + roomWidth;

            if( spawnThreadhold < startRoomPosX)
            {
                canLoadRoom = false;
            }

            if( despawnThreadhold > endRoomPosX)
            {
                removeRooms.Add(room);
            }

            furthestRoomEndX = Mathf.Max(endRoomPosX, furthestRoomEndX);
        }

        //Remove the rooms 
        foreach( var room in removeRooms)
        {
            currentRooms.Remove(room);
            Destroy(room);
        }

        //Spawn the next room if posible
        if(canLoadRoom) 
        {
            AddRoom(furthestRoomEndX);
        }
    }

    

}
