using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerController : MonoBehaviour
{
    public Sprite laserOnSprite;
    public Sprite laserOffSprite;

    public float toggleInterval = 0.5f;
    public float rotationSpeed = 0.0f;

    private bool _isLaserOn = true;
    private float _timeUntilNextToggle;

    private Collider2D _collider;
    private SpriteRenderer _spriteRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        _timeUntilNextToggle = toggleInterval;

        _collider = GetComponent<Collider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //Countdown timer
        _timeUntilNextToggle -= Time.deltaTime;

        if (_timeUntilNextToggle <= 0)
        {
            _isLaserOn = !_isLaserOn;
            _collider.enabled = _isLaserOn;
            _spriteRenderer.sprite = _isLaserOn ? laserOnSprite : laserOffSprite;

            _timeUntilNextToggle = toggleInterval;
        }
        
        transform.RotateAround(this.transform.position, Vector3.forward, rotationSpeed * Time.deltaTime);
    }
}
