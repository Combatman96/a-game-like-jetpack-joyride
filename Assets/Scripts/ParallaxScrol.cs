using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScrol : MonoBehaviour
{
    public Renderer foreground;

    public Renderer background;

    public float foregroundSpeed = 0.02f;
    public float backgroundSpeed = 0.06f;

    public float offset = 0f;
    

    // Update is called once per frame
    void Update()
    {
        float backgroundOffset = offset * backgroundSpeed;
        float foregroundOffset = offset * foregroundSpeed;
        
        background.material.mainTextureOffset = new Vector2(backgroundOffset, 0);
        foreground.material.mainTextureOffset = new Vector2(foregroundOffset, 0);
    }
}
