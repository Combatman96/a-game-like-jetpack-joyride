using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    public Animator  startBtn;
    public Animator settingBtn;

    public Animator settingDialog;

    public Animator slideMenu;
    private bool slideMenuIsHidden = true;
    public Animator gearImg;

    public void StartGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void OpenSetting()
    {
        startBtn.SetBool("isHidden", true);
        settingBtn.SetBool("isHidden", true);
        
        settingDialog.SetBool("isHidden", false);
    }
    
    public void CloseSetting()
    {
        startBtn.SetBool("isHidden", false);
        settingBtn.SetBool("isHidden", false);
        
        settingDialog.SetBool("isHidden", true);
    }

    public void ToggleSlideMenu()
    {
        slideMenuIsHidden = !slideMenuIsHidden;
        slideMenu.SetBool("isHidden", slideMenuIsHidden);
        gearImg.SetBool("isHidden", slideMenuIsHidden);
    }
    
}
