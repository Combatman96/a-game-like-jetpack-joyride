using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameHandler : MonoBehaviour
{
    public GameObject endMenu;

    private void Start()
    {
        endMenu.SetActive(false);
    }
    
    public void ShowMenu()
    {
        endMenu.SetActive(true);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("RocketMice", LoadSceneMode.Single);
    }

    public void BackToTitle()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
